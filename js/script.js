"use strict";
console.log("script.js 1.1");

function InitPagina() {
    const listarBtn = document.querySelector('#link-listado');
    const nuevaFacturaBtn = document.querySelector('#link-nueva');

    listarBtn.addEventListener('click', e => {
        InitListado();
    });

    nuevaFacturaBtn.addEventListener('click', e => {
        initNuevaFactura();
    });

    class Factura {
        // Declarar claves
        cliente = "";
        fecha = "";
        id = "";
        id_cliente = "";
        importe = 0;
        items = [];
        // Contenedores 
        contenedorGeneral = document.createElement("div");
        contenedorDatosCliente = document.createElement("div");
        contenedorItems = document.createElement("div");

        constructor(factura) {
            this.cliente = factura.cliente;
            this.fecha = factura.fecha;
            this.id = factura.id;
            this.id_cliente = factura.id_cliente;
            this.importe = factura.importe;

            //añadir datos del cliente al contenedor
            this.contenedorDatosCliente.innerHTML += `<span class="nombre-cliente">${ this.cliente } </span>`;
            this.contenedorDatosCliente.innerHTML += `<span class="fecha">${ this.fecha } </span>`;
            this.contenedorDatosCliente.innerHTML += `<span class="id-factura">${ this.id } </span>`;
            this.contenedorDatosCliente.innerHTML += `<span class="edad-alumno">${ this.importe } </span>`;

            factura.items.forEach(item => {
                this.items.push(item);
                //añadir los items al contenedor
                this.contenedorItems.innerHTML += `<span class="item-nombre">${ item.nombre_producto } </span>`;
                this.contenedorItems.innerHTML += `<span class="item-importe">${ item.importe } </span>`;
                this.contenedorItems.innerHTML += `<span class="item-unidad">${ item.unidades } </span>`;
            });
            this.contenedorGeneral.append(this.contenedorDatosCliente, this.contenedorItems);
        }
    };

    class Formulario {
        contenedorGeneral = document.createElement("div");
        contenedorBtn = document.createElement("div");

        formNuevaFactura = document.createElement("form");
        headerNuevaFactura = document.createElement("header");
        hrNuevaFactura = document.createElement("hr");

        inputEnviar = document.createElement("input");
        buttonEliminar = document.createElement("button");
        lista_productos = [];

        constructor() {
            this.contenedorGeneral.classList.add("row-nueva-factura");
            this.formNuevaFactura.id = "form-concepto"
            this.inputEnviar.type = "submit";
            this.inputEnviar.value = "Guardar";
            this.inputEnviar.id = "Enviar"
            this.buttonEliminar.textContent = "Eliminar";
            this.buttonEliminar.id = "eliminar-btn"
        }

        setNuevoProducto(nuevoProducto) {
            let contenedorFactura = document.createElement("div");
            contenedorFactura.id = this.lista_productos.length > 0
                ? parseInt(this.lista_productos[this.lista_productos.length - 1].id) + 1
                : 0;

            this.lista_productos.push(contenedorFactura)

            let contenedorTitulo = document.createElement("div")
            let contenedorInputs = document.createElement("div")
            contenedorTitulo.id = `titulo_producto_${ contenedorFactura.id }`
            contenedorInputs.id = `inputs_producto_${ contenedorFactura.id }`

            contenedorTitulo.append(
                nuevoProducto.fieldSetProducto.textContent,
                nuevoProducto.fieldSetImporte.textContent,
                nuevoProducto.fieldSetUnidades.textContent,
                document.createElement("hr")
            );

            let eliminar_factura = document.createElement("button")
            eliminar_factura.textContent = "Eliminar";
            eliminar_factura.id = `${ contenedorFactura.id }_eliminar`
            eliminar_factura.addEventListener("click", () => {
                this.eliminarProducto(contenedorFactura)
            });

            contenedorInputs.append(
                nuevoProducto.inputProducto,
                nuevoProducto.inputImporte,
                nuevoProducto.inputUnidades,
                eliminar_factura
            );

            contenedorFactura.append(
                contenedorTitulo,
                contenedorInputs
            );

            this.headerNuevaFactura.append(contenedorFactura)
            this.formNuevaFactura.append(
                this.headerNuevaFactura,
                this.hrNuevaFactura
            );
            this.contenedorGeneral.append(
                this.formNuevaFactura
            );
        }

        eliminarProducto(contenedorFactura) {
            this.lista_productos = this.lista_productos.filter(
                item => item.id !== contenedorFactura.id
            );
            this.headerNuevaFactura.removeChild(contenedorFactura);
            if (this.lista_productos.length === 0) this.inputEnviar.parentNode.removeChild(this.inputEnviar)
        }

        enviarFormulario() {
            this.lista_productos.map(producto => {
                console.log(producto)
            })
        }
    }

    class NuevoProducto {
        fieldSetProducto = document.createElement("fieldset");
        fieldSetImporte = document.createElement("fieldset");
        fieldSetUnidades = document.createElement("fieldset");

        labelProducto = document.createElement("label");
        labelImporte = document.createElement("label");
        labelUnidades = document.createElement("label");

        inputProducto = document.createElement("input");
        inputImporte = document.createElement("input");
        inputUnidades = document.createElement("input");

        constructor() {
            this.labelProducto.textContent = "Producto";
            this.labelImporte.textContent = "Importe";
            this.labelUnidades.textContent = "Unidades";

            this.inputProducto.name = "producto";
            this.inputImporte.name = "importe";
            this.inputUnidades.name = "unidades";

            this.fieldSetProducto.append(this.labelProducto, this.inputProducto);
            this.fieldSetImporte.append(this.labelImporte, this.inputImporte);
            this.fieldSetUnidades.append(this.labelUnidades, this.inputUnidades);
        }
    }

    //comenzar a listar
    const mainContainer = document.querySelector('#container-facturas');
    function InitListado() {
        mainContainer.innerHTML = "";
        getFacturas();

        function getFacturas() {
            fetch(apiUrlFacturasGet, { method: "GET" })
                .then(respuesta => {
                    if (!respuesta.ok) {
                        throw new Error('Hay un error' + respuesta.status);
                    }
                    respuesta.json().then(datos => {
                        console.log(datos);
                        printFacturas(datos);
                    });
                })
                .catch(error => {
                    console.log(error);
                });
        }

        function printFacturas(facturas) {
            facturas.forEach(factura => {
                const nuevaFactura = new Factura(factura);
                mainContainer.append(nuevaFactura.contenedorGeneral);
            });
        }
    }

    function initNuevaFactura() {
        const main_form = new Formulario();
        mainContainer.append(main_form.formNuevaFactura);
        mainContainer.append(main_form.contenedorGeneral);

        main_form.formNuevaFactura.addEventListener("submit", (e) => {
            e.preventDefault()
            main_form.enviarFormulario()
        })

        //Limpiar contenedor
        mainContainer.innerHTML = "";
        getClientes();

        function getClientes() { };

        const nuevoConceptoBtn = document.createElement("button");
        nuevoConceptoBtn.textContent = "Nuevo Concepto";
        mainContainer.append(nuevoConceptoBtn);

        nuevoConceptoBtn.addEventListener('click', e => {
            if(main_form.lista_productos.length === 0) {
                main_form.formNuevaFactura.append(
                    main_form.inputEnviar,
                );
            }
            let nuevo_producto = new NuevoProducto();
            main_form.setNuevoProducto(nuevo_producto)
            mainContainer.append(main_form.contenedorGeneral);
        });
    }
}

InitPagina();